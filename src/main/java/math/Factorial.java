package math;

import java.util.HashMap;
import java.util.Map;

public class Factorial {

    private static final int DEFAULT_CACHE_LENGTH = 100;

    private static final Map<Integer, Long> cache = new HashMap<>(DEFAULT_CACHE_LENGTH);

    public static long factorialWithCache(int n) {
        Long result;

        if (n == 0) {
            return 1;
        }
        if ((result = cache.get(n)) != null) {
            return result;
        }
        result = n * factorialWithCache(n - 1);
        cache.put(n, result);
        return result;
    }

}
