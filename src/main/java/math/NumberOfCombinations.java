package math;

import static math.Factorial.factorialWithCache;

public class NumberOfCombinations {

    public static long numberOfCombination(int n, int k) {
        return factorialWithCache(n) / factorialWithCache(k) / factorialWithCache(n - k);
    }

}
