import java.util.InputMismatchException;
import java.util.Scanner;

import static core.Algorithm.find;

public class Main {

    public static void main(String[] args) {
        try (Scanner sc = new Scanner(System.in)) {
            while (true) {
                try {
                    System.out.print("Введите число солдат: ");
                    int input = sc.nextInt();

                    if (input < 3) {
                        System.out.println("Число возможных неправильных шеренг равно 0");
                        return;
                    }

                    break;
                } catch (InputMismatchException e) {
                    System.out.println("Некорректный ввод. Напишете только целое число.");
                    sc.nextLine();
                } catch (ArithmeticException | StackOverflowError e) {
                    System.out.println("Возможно число, которые вы ввели, слишком большое. Попробуйте ввести число поменьше.");
                }
            }
        }
    }

}
