package core;

import static math.NumberOfCombinations.numberOfCombination;

public class Algorithm {

    public static long find(int n) {

        long result = 0;

        if (n == 0 || n == 1) {
            return 2;
        }

        for (int k = 0; k <= n - 1; k++) {
            result += numberOfCombination(n - 1, k) * find(k) * find(n - 1 - k);
        }

        return result / 4;
    }

}
