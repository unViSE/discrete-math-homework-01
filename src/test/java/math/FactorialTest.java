package math;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static math.Factorial.factorialWithCache;
import static org.junit.jupiter.api.Assertions.assertEquals;

class FactorialTest {

    @Test
    public void factorialWithCacheTest() {
        Map<Integer, Long> expectedValues = Map.of(
                0, 1L, 1, 1L, 2, 2L, 3, 6L, 4, 24L
        );

        expectedValues.forEach((k, v) -> assertEquals(factorialWithCache(k), v));
    }

}