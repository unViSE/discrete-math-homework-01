package math;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static math.NumberOfCombinations.numberOfCombination;
import static org.junit.jupiter.api.Assertions.assertEquals;

class NumberOfCombinationsTest {

    @Test
    public void numberOfCombinationTest() {
        Map<List<Integer>, Long> inputValues = Map.of(
                List.of(5, 3), 10L,
                List.of(0, 0), 1L,
                List.of(7, 3), 35L,
                List.of(4, 2), 6L
        );

        inputValues.forEach((k, v) -> assertEquals(numberOfCombination(k.get(0), k.get(1)), v));

    }

}