package core;

import org.junit.jupiter.api.Test;

import java.util.Map;

import static core.Algorithm.find;
import static org.junit.jupiter.api.Assertions.assertEquals;

class AlgorithmTest {

    @Test
    public void findTest() {
        Map<Integer, Long> expectedValues = Map.of(
                0, 2L, 1, 2L, 3, 4L, 4, 10L, 5, 32L
        );

        expectedValues.forEach((k, v) -> assertEquals(find(k), v));
    }

}